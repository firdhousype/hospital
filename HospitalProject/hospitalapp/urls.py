from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('',views.listpatients,name='listpatients'),
    path('searchPatients',views.searchPatients,name='searchPatients'),
    path('retrievePatientsByDepartment/<str:department>',views.retrievePatientsByDepartment,name='retrievePatientsByDepartment'), 
    path('retrievePatientsById/<int:patientId>',views.retrievePatientsById,name='retrievePatientsById')

]