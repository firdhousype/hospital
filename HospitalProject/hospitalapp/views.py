from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse,HttpResponseRedirect
from rest_framework.response import Response 
from rest_framework.decorators import api_view,permission_classes
from .serializers import *
from .models import *
from django.db.models import Q
@api_view(['GET'])
def retrievePatientsByDepartment(request,department):
    if  Patients.objects.filter(department__departmentName__startswith=department).exists():
        patient     =   Patients.objects.filter(department__departmentName__startswith=department)
        serializer  =   patientserializer(patient,many=True)
        return Response(serializer.data)
    else:
        data    =   {"Status":"False","messege ":"Invalid Department name"}
        return Response(data)
@api_view(['GET'])
def retrievePatientsById(request,patientId):
    if  Patients.objects.filter(id=patientId).exists():
        patient     =   Patients.objects.filter(id=patientId)
        serializer  =   patientserializer(patient,many=True)
        return Response(serializer.data)
    else:
        data    =   {"Status":"False","messege ":"Invalid Department name"}
        return Response(data)
########### search patients information ############
def listpatients(request):
    result  =   Patients.objects.all().order_by('-id')
    return render(request,'patients.html',{'result':result})
def searchPatients(request):
    result  =   Patients.objects.all().order_by('-id')
    if request.method == 'POST':
        search  =   request.POST.get('searchVariable')
        result  =   Patients.objects.filter(name__startswith=search).order_by('-id')
        return render(request,'patients.html',{'result':result})
