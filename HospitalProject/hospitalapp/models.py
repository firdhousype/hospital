from django.db import models

# Create your models here.
class Department(models.Model):
    departmentName     =   models.CharField(max_length=250,null=True,blank=True)
    def __str__(self):
        return str(self.departmentName)
class Doctor(models.Model):
    department      =   models.ForeignKey(Department,on_delete=models.CASCADE,null=True,blank=True)
    employmentId    =   models.CharField(max_length=50,null=True,blank=True)
    def __str__(self):
        return str(self.employmentId)
class Nurse(models.Model):
    department      =   models.ForeignKey(Department,on_delete=models.CASCADE,null=True,blank=True)
    employmentId    =   models.CharField(max_length=50,null=True,blank=True)
    def __str__(self):
        return str(self.employmentId)
class Patients(models.Model):
    name        =   models.CharField(max_length=100,null=True,blank=True)
    age         =   models.IntegerField()
    doctor      =   models.ForeignKey(Doctor,on_delete=models.CASCADE,null=True,blank=True)
    department  =   models.ForeignKey(Department,on_delete=models.CASCADE,null=True,blank=True)
    def __str__(self):
        return str(self.name)