from rest_framework import serializers
from .models import *


class departmentserializer(serializers.ModelSerializer):
    class Meta:
        model   =   Department
        fields  =   [
            'id',
            'departmentName',
        ]
class doctorserializer(serializers.ModelSerializer):
    department  =   departmentserializer()
    class Meta:
        model   =   Doctor
        fields  =   [
            'id',
            'employmentId',
            'department',
        ]
class patientserializer(serializers.ModelSerializer):
    doctor      =   doctorserializer()
    department  =   departmentserializer()
    class Meta:
        model   =   Patients
        fields  =   [
            'id',
            'name',
            'age',
            'doctor',
            'department',
        ]